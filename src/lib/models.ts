export type User = {
	username: string;
	last_name: string;
	first_name: string;
	email: string;
	password: string;
};

export type Note = {
	id: number;
	username: string;
	text: string;
};
