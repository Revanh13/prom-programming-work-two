import type { Note, User } from "$lib/models";
import prisma from "./prisma";

export async function AddUser(item: User): Promise<boolean> {
	try {
		const form = await prisma.user.create({
			data: { ...item },
		});
		return true;
	} catch (error) {
		console.log(error);
		return false;
	}
}

export async function CheckUser(username: string, password: string): Promise<boolean> {
	try {
		const res = await prisma.user.findUnique({
			where: {
				username: username,
				password: password,
			},
		});
		if (res) return true;
		return false;
	} catch (error) {
		console.log(error);
		return false;
	}
}

export async function GetNotes(username: string): Promise<Note[] | undefined> {
	try {
		return await prisma.note.findMany({
			where: {
				username: {
					equals: username,
				},
			},
		});
	} catch (error) {
		console.log(error);
		return undefined;
	}
}

export async function AddNote(username: string, text: string): Promise<boolean> {
	try {
		const form = await prisma.note.create({
			data: { username: username, text: text },
		});
		return true;
	} catch (error) {
		console.log(error);
		return false;
	}
}
