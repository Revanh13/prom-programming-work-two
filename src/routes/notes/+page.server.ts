import { redirect } from "@sveltejs/kit";
import type { PageServerLoad } from "./$types";
import { GetNotes } from "$lib/server";
import type { Note } from "$lib/models";

export const load = (async ({ cookies }) => {
	const username = cookies.get("username");

	if (!username) throw redirect(303, "/login");

	const notes = await GetNotes(username);

	if (notes == undefined) return { notes: new Array<Note>() };

	return { notes };
}) satisfies PageServerLoad;
