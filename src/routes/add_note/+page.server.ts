import { redirect, type Actions, fail } from "@sveltejs/kit";
import type { PageServerLoad } from "./$types";
import { AddNote } from "$lib/server";

export const load = (async ({ cookies }) => {
	const username = cookies.get("username");

	if (!username) throw redirect(303, "/login");
}) satisfies PageServerLoad;

export const actions = {
	default: async ({ cookies, request }) => {
		const data = await request.formData();

		const username = cookies.get("username");
		const text = data.get("text");

		const res = await AddNote(username!, text!.toString());

		if (res == false)
			return fail(400, { success: false, message: "Ошибка при добавлении заметки!" });

		throw redirect(301, "/notes");
	},
} satisfies Actions;
