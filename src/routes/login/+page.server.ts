import { fail, redirect } from "@sveltejs/kit";
import type { Actions, PageServerLoad } from "./$types";
import { CheckUser } from "$lib/server";

export const load = (async ({ cookies }) => {
	const username = cookies.get("username");

	if (username) throw redirect(303, "/notes");
}) satisfies PageServerLoad;

export const actions = {
	login: async ({ cookies, request }) => {
		const data = await request.formData();

		const username = data.get("username");
		const password = data.get("password");

		const res = await CheckUser(username!.toString(), password!.toString());

		if (res == false)
			return fail(400, { success: false, message: "Неверный логин или пароль!" });

		cookies.set("username", username!.toString(), {
			path: "/",
			httpOnly: true,
			secure: false,
			sameSite: "strict",
		});

		throw redirect(301, "/notes");
	},
	logout: async ({ cookies }) => {
		cookies.delete("username", {
			path: "/",
			httpOnly: true,
			secure: false,
			sameSite: "strict",
		});

		throw redirect(303, "/login");
	},
} satisfies Actions;
