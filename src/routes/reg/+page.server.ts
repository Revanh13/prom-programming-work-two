import { fail, redirect } from "@sveltejs/kit";
import type { Actions, PageServerLoad } from "./$types";
import type { User } from "$lib/models";
import { AddUser } from "$lib/server";

export const load = (async ({ cookies }) => {
	const username = cookies.get("username");

	if (username) throw redirect(303, "/notes");
}) satisfies PageServerLoad;

export const actions = {
	default: async ({ cookies, request }) => {
		const data = await request.formData();

		const username = data.get("username");
		const first_name = data.get("first_name");
		const last_name = data.get("last_name");
		const email = data.get("email");
		const password1 = data.get("password1");
		const password2 = data.get("password2");

		if (password1 != password2)
			return fail(400, { success: false, message: "Пароли не совпадают!" });

		const user: User = {
			username: username!.toString(),
			last_name: last_name!.toString(),
			first_name: first_name!.toString(),
			email: email!.toString(),
			password: password1!.toString(),
		};

		const res = await AddUser(user);

		if (res == false)
			return fail(400, {
				success: false,
				message: "Ошибка при регистрации пользователя в базе!",
			});

		cookies.set("username", username!.toString(), {
			path: "/",
			httpOnly: true,
			secure: false,
			sameSite: "strict",
		});

		throw redirect(301, "/notes");
	},
} satisfies Actions;
