import type { LayoutServerLoad } from "./$types";

export const load = (async ({ cookies }) => {
	const username = cookies.get("username");

	if (!username) return { auth: false };
	return { auth: true };
}) satisfies LayoutServerLoad;
